#include <stdio.h>
#include <windows.h>
#include <time.h> 
//se importan librerias necesarias, de windows seusa sleep y de time se usan varias para registro de instantes
int main()
{
    time_t now = time(NULL), horaInicio = time(NULL);//variables de control de tiempo
    unsigned int hertz, SEGUNDOS_PROCESAMIENTO = 3600, segundosTranscurridos = difftime(now, horaInicio); //define cuánto tiempo se estarán generando DATOS
    FILE *archivo = fopen("FreqReq.txt", "a"); //archivo que va a guardar los datos, se abre en modo append para que si no existe lo crea
    char horaString[20]; 

    fprintf(archivo, "Hora\t\t\t\t|\tSeñal\n");//
    fprintf(archivo, "---------------------------\n");

    strftime(horaString, 20, "%Y-%m-%d %H:%M:%S", localtime(&now)); //se guarda una hora en el formato indicado con el largo de caracteres espcificado
    printf("Se registraran datos durante %i segundos\n", SEGUNDOS_PROCESAMIENTO);
    printf("Inicio: %s\n", horaString);
    /*Mientras el tiempo transcurrido desde el inicio del prrograma sea menor al tiempo designado para generar datos
    now guarda la hora del momento, luego la funcion strftime modifica el string "horastring" con la nueva hora 
    hertz define una frecuencia random entre 1 y 500 hz y luego se escribe en el archivo 
    En segundostranscurridos se guarda nuevamente la diferenca entre el tiempo que ha pasado y el tiempo de generacion de datos
    Luego la funcion sleep genera el iempo de espera con la frecuencia creada en Herzt, multiplicado por 1000 ya que la funcion trabaja
    en milisegundos */
    while (segundosTranscurridos < SEGUNDOS_PROCESAMIENTO) 
    {
        now = time(NULL);
        strftime(horaString, 20, "%Y-%m-%d %H:%M:%S", localtime(&now));

        hertz = ((rand() % 499) + 1) * 1;
        fprintf(archivo, "%s :\t%d\t\n", horaString, hertz);

        segundosTranscurridos = difftime(now, horaInicio);
        printf("\rFaltan %i segundos", SEGUNDOS_PROCESAMIENTO - segundosTranscurridos);

        Sleep(1000 / hertz);
    }
    printf("\nTermino: %s\n", horaString);

    printf("\nLISTO!", horaString);
    fclose(archivo);

    return (0);
}