#EJERCICIO 2 PRUEBA RECUPERATIVA SIST. EMBEBIDOS PUCV - NICOLÁS FUENTES C.
import traducir as funcion
print("ingrese el numero a traducir: ")
c=0
#peticion de entrada y verificacion de que la entrada sea un numero
while c==0:

    N=input('N= ')
    try:
        N=int(N) #probar esta igualdad
        if N<0 or N>99:#si la igualdad es true entra a este if
            raise Exception()#si es un numero que no está entre 0 y 99 salta al error del try
        c=1
        print("ok")
    except:
        print("Ha introducido un valor no válido.")
        continue
print(f"el numero {N} en chino es: {funcion.traducir(N)}")


