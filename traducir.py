#EJERCICIO 2 PRUEBA RECUPERATIVA SIST. EMBEBIDOS PUCV - NICOLÁS FUENTES C.
def traducir(N):
    #el diccionario lo entregan en el enunciado
    numeros={'0':'ling','1':'yi','2':'er','3':'san','4':'si','5':'wu','6':'liu','7':'qi','8':'ba','9':'jiu','10':'shi'}
    if int(N)<=10: #condiciones para N de cero a diez
        traduccion=numeros[N]
    elif int(N)<20: #condiciones para N entre diez y 19
        digitodos=int(N)-10
        digitodostraducido=numeros[str(digitodos)]
        traduccion="shi "+digitodostraducido
    elif int(N)<100: #condiciones para N entre 20 y 99
        digitouno=int(0.1*int(N))
        digitounotraducido=numeros[str(digitouno)]
        digitodos=int(N)-10*digitouno
        digitodostraducido=numeros[str(digitodos)]
        traduccion=digitounotraducido+" shi "+digitodostraducido
    return traduccion


